**How do I play the game?**

Strip Poker Night at the Inventory is an online game currently hosted at https://spnati.gitlab.io/. If you ever forget where it is, you can always follow the link on the subreddit sidebar.

**Can I play the game offline?**

Yes! You can download the game from https://gitlab.com/spnati/spnati.gitlab.io. After downloading, simply open the index.html file with a web browser - we recommend Firefox.

**Why can't I select characters when I play offline?**

Some browsers seem to have problems with the game when playing offline, notably Chrome. We recommend playing with Firefox for that reason.

**How do I get endings?**

To see one of the character endings, you need to:
1) Beat EVERY opponent in the game, and:
2) Play against at least one opponent who has an epilogue. 

Sadly, most opponents don't have endings. It's not that hard to make one, though - if your favorite character doesn't have one, why not make it?

**How do I add the characters I see posted on Reddit?**

All the characters who are finished are already in the game! We accept any character as long as they meet the rules of the game: no underage characters and no real people. However, characters only get added when they're complete. Many of the posts you see in the subreddit are people who have made a model of a character, but haven't made any dialogue for the character - which means they wouldn't be able to say anything in the game.

In the end, if you want to add one of these characters to the game, your best bet is to volunteer to write their dialogue!

**Why are there so few male opponents?**

SPNATI is a fan-made project. Every character is made by a player who decided they wanted that character in the game. It turns out that most of the players want to play against girls... but there's nothing at *all* preventing players from making male characters if they want!

**Can I request a character?**

Sure you can! There's a character suggestion thread here: https://www.reddit.com/r/spnati/comments/4ubucc/suggestions_for_new_characters/

It won't do anything, but you can request anything you want. Dozens of characters get requested and we'll dutifully add their names to the list. But if you want to see them get made, the best thing to do is to make them yourself.

**Can I make a character?**

Yes! We'd love you to. It's actually quite easy to make a character, just very time-consuming, and the players who are most interested in a character are usually the ones who are best suited to making them. It does not require any ability to code or make art.

There's a beginner's walkthrough here: https://www.reddit.com/r/spnati/comments/5vsfas/the_beginners_guide_to_making_characters_for/

There's also a more advanced walkthrough here: https://www.reddit.com/r/spnati/comments/4zboc7/how_to_make_a_character/

**I can't do art. Can I still make a character?**

Yes. We use a program called **kisekae** to make characters. Kisekae is the Japanese for 'paper doll,' and the program lets you make characters, dress them and pose them to your heart's content. 

**I can't code. Can I still make a character?**

Yes. No coding is necessary! Characters require poses (using kisekae) and dialogue (filling in blanks in a text file) - no coding needed.



